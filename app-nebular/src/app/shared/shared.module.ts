import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderConvocatoriaComponent } from './header-convocatoria/header-convocatoria.component';
import { HeaderAdminComponent } from './header-admin/header-admin.component';
import { MenuAdminComponent } from './menu-admin/menu-admin.component';

import { NbThemeModule, NbLayoutModule, NbSidebarModule, NbCardModule, NbButtonModule, NbActionsModule, NbIconModule, NbFormFieldModule, NbUserModule, NbSpinnerModule, NbToastrModule, NbInputModule, NbStepperModule, NbAlertModule, NbDialogModule, NbBadgeModule, NbTooltipModule, NbCalendarRangeModule, NbCalendarModule, NbListModule, NbTabsetModule, NbMenuModule, NbRadioModule, NbSelectModule, NbCheckboxModule, NbAccordionModule, NbContextMenuModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { NgxSpinnerModule } from "ngx-spinner";
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
	declarations: [HeaderConvocatoriaComponent, HeaderAdminComponent, MenuAdminComponent],
	imports: [
		CommonModule,
		NbThemeModule.forRoot({ name: 'corporate' }),
		NbLayoutModule,
		NbEvaIconsModule,
		NgxSpinnerModule,
		NbSidebarModule.forRoot(),
		NbCardModule,
		NbButtonModule,
		NbActionsModule,
		NbIconModule,
		HttpClientModule,
		NbFormFieldModule,
		FormsModule,
		ReactiveFormsModule,
		NbUserModule,
		NbSpinnerModule,
		NbToastrModule.forRoot({
			duration: 5000,
		}),
		NbContextMenuModule,
		NbInputModule,
		NbStepperModule,
		NbAlertModule,
		NbDialogModule.forRoot({
			closeOnBackdropClick : false
		}),
		NbBadgeModule,
		NbTooltipModule,
		NbCalendarRangeModule,
		NbCalendarModule,
		NbListModule,
		NbTabsetModule,
		NbMenuModule.forRoot(),
		NbRadioModule,
		NbSelectModule,
		NbCheckboxModule,
		NbAccordionModule,

	],
	exports:[
		HeaderConvocatoriaComponent,
		HeaderAdminComponent,
		MenuAdminComponent
	]
})
export class SharedModule { }
