import { Component, OnInit } from '@angular/core';
import { NbToastrService, NbMenuItem } from '@nebular/theme';

@Component({
	selector: 'app-menu-admin',
	templateUrl: './menu-admin.component.html',
	styleUrls: ['./menu-admin.component.scss']
})
export class MenuAdminComponent implements OnInit {

	items: NbMenuItem[] = [
		{
			title: 'Convocatorias',
			expanded: true,
			children: [
			{
				title: 'Listado',
				link: '/convocatorias/interna',
			}],
		}
	];

	constructor() { }

	ngOnInit(): void {
	}

}
