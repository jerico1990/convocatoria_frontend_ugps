import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NbMenuService } from '@nebular/theme';
import { filter, map } from 'rxjs/operators';

@Component({
  selector: 'app-header-admin',
  templateUrl: './header-admin.component.html',
  styleUrls: ['./header-admin.component.scss']
})
export class HeaderAdminComponent implements OnInit {

  userMenu = [ { title: 'Salir' } ];
  usuario = 'Juan Perez'
  users:any 
  user = null;
  userPictureOnly: boolean = false;

  constructor(
    private router  : Router, 
    private menuService: NbMenuService,
  ) { 
    this.usuario = sessionStorage.getItem('usuario')
  }

  ngOnInit(): void {
    this.menuService.onItemClick()
      .pipe(
        filter(({ tag }) => tag === 'my-context-menu'),
        map(({ item: { title } }) => title),
      )
      .subscribe( 
        title => {
          if(title == 'Salir'){
            this.salir()
            console.log(`${title} was clicked!`) 
          }
        }
      );
  }

  salir(){
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('usuario');
    this.router.navigate(["/"]);
  }

  navigateHome() {
		this.menuService.navigateHome();
		return false;
	}

}
