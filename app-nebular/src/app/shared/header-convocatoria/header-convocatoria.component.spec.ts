import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderConvocatoriaComponent } from './header-convocatoria.component';

describe('HeaderConvocatoriaComponent', () => {
  let component: HeaderConvocatoriaComponent;
  let fixture: ComponentFixture<HeaderConvocatoriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderConvocatoriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderConvocatoriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
