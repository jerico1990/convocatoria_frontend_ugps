import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NbToastrService } from '@nebular/theme';
import { NgxSpinnerService } from "ngx-spinner";
import { Router, ActivatedRoute } from '@angular/router';
import { ConvocatoriaService } from 'src/app/service/convocatoria.service';
import { PostulanteService } from 'src/app/service/postulante.service';
import { FileValidator } from 'src/app/helper/FileValidator';
import { HttpErrorResponse } from '@angular/common/http';
import Swal from 'sweetalert2/dist/sweetalert2.js';
@Component({
	selector: 'app-registro-postulante',
	templateUrl: './registro-postulante.component.html',
	styleUrls: ['./registro-postulante.component.scss']
})
export class RegistroPostulanteComponent implements OnInit {

	siteKey: string = "6LeiOQQbAAAAAKq7YX84yGb5h3nz4PHco7pqX6lp"
	grupoFiltro: FormGroup;
	submitted = false;
	listConvocatorias = []
	listEstado = []
	selectedItem
	codigo: any;

	datosConvocatoria = {
		nombre: "",
		codigo: "",
		correlativo: ""
	}

	filesData: any = {
		formato: '',
		documentado: '',
		comercial: ''
	}
	allowedExtensions: any
	defaultPeso = 2;

	listRegion = []
	listProvincia = []
	listDistrito = []

	constructor(
		private formBuilder: FormBuilder,
		private toastrService: NbToastrService,
		private spinner: NgxSpinnerService,
		private router: Router,
		private route: ActivatedRoute,
		private convocatoria: ConvocatoriaService,
		private postulante: PostulanteService,
	) { }

	ngOnInit(): void {
		this.codigo = this.route.snapshot.paramMap.get('id');
		this.loadData(this.codigo)
		this.validFormulario()
		this.validaNumeroci()
		this.listaRegion()
	}

	loadData(id) {
		this.spinner.show()
		this.convocatoria.listaConvocatoriaId({ id: id }).subscribe((data: any) => {
			// console.log(data)
			if (data.respuesta = 'OK') {
				this.datosConvocatoria.nombre = data.datos[0].txtDescripcion
				this.datosConvocatoria.codigo = data.datos[0].codConvocatoria
				this.datosConvocatoria.correlativo = data.datos[0].txtCorrelativo

				if (data.datos[0].intEstadoTrazabilidad == 3) {
					this.router.navigate(['convocatorias/publicas']);
				}
			}
			this.spinner.hide();
		},
			(error) => {
				console.log(error)
			});
	}

	validFormulario() {

		this.allowedExtensions = ['pdf'];
		// console.log(FileValidator.fileExtensions(this.allowedExtensions))
		this.grupoFiltro = this.formBuilder.group({
			nro_doc: ['', [Validators.maxLength(8), Validators.required]],
			nombres: ['', Validators.required],
			apellidos: ['', Validators.required],
			email: ['', [Validators.email, Validators.required]],
			celular: ['', Validators.required],
			direccion: ['', Validators.required],

			region: ['', Validators.required],
			provincia: ['', Validators.required],
			distrito: ['', Validators.required],

			formato: ['', [FileValidator.fileMinSize(1), FileValidator.fileExtensions(this.allowedExtensions), Validators.required]],
			cv_comercial: ['', [FileValidator.fileMinSize(1), FileValidator.fileExtensions(this.allowedExtensions), Validators.required]],
			cv_documentado: ['', [FileValidator.fileMinSize(1), FileValidator.fileExtensions(this.allowedExtensions), Validators.required]],
			recaptcha: ['', Validators.required]
		});


	}
	get f() {
		// console.log(this.grupoFiltro.controls)
		return this.grupoFiltro.controls;
	}

	registroPostulante() {
		this.spinner.show();
		this.submitted = true;
		if (!this.grupoFiltro.invalid) {

			Swal.fire({
				title: '¿Esta seguro de enviar la información?',
				text: 'Una vez enviada no podrá realizar nuevamente la postulación',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Si, Enviar',
				cancelButtonText: 'No'
			}).then((result) => {
				if (result.value) {
					this.spinner.show();
					let codConvocatoria = this.datosConvocatoria.codigo
					let txtNroDocumento = this.grupoFiltro.controls['nro_doc'].value
					let txtNombres = this.grupoFiltro.controls['nombres'].value
					let txtApellidos = this.grupoFiltro.controls['apellidos'].value
					let txtCorreoElectronico = this.grupoFiltro.controls['email'].value
					let txtCelular = this.grupoFiltro.controls['celular'].value
					let txtDireccion = this.grupoFiltro.controls['direccion'].value

					let txtRegion = this.grupoFiltro.controls['region'].value
					let txtProvincia = this.grupoFiltro.controls['provincia'].value
					let txtDistrito = this.grupoFiltro.controls['distrito'].value
		
					let fileInput = this.validaExisteFile()
					console.log(fileInput)
					if(fileInput)
					{
						const formData = new FormData()
						formData.append('codConvocatoria', codConvocatoria)
						formData.append('txtNroDocumento', txtNroDocumento)
						formData.append('txtNombres', txtNombres)
						formData.append('txtApellidos', txtApellidos)
						formData.append('txtCorreoElectronico', txtCorreoElectronico)
						formData.append('txtCelular', txtCelular)
						formData.append('txtDireccion', txtDireccion)

						formData.append('txtRegionId', txtRegion)
						formData.append('txtProvinciaId', txtProvincia)
						formData.append('txtDistritoId', txtDistrito)

						formData.append('fileFormatosArchivo', this.filesData.formato)
						formData.append('fileCvComercialArchivo', this.filesData.comercial)
						formData.append('fileCvDocumentadoArchivo', this.filesData.documentado)
			
						this.postulante.registroPostulante(formData).subscribe((data: any) => {
							// console.log(data)
							if (data.respuesta == 'OK') {
								if(data.datos == 0)
								{
									this.toastrService.danger("Este numero de documento ya ha sido registrado en esta convocatoria", "POSTULANTE");
								}else{
									Swal.fire(
										'REGISTRO CONVOCATORIA!',
										'Se ha registrado correctamente en la convocatoria!',
										'success'
									)
									setTimeout(() => {
										this.router.navigate(['convocatorias/publicas']);
									}, 3000);
								}
								//this.router.navigate(['convocatorias/publicas']);
							} else {
								this.toastrService.danger("Ocurrio un error: " + data.mensaje, "POSTULANTE");
							}
							this.spinner.hide();
						},
						(error) => {
							// console.log(error)
							if (error instanceof HttpErrorResponse) {
								if (error.error instanceof ErrorEvent) {
									console.error("Error Event");
								} else {
									console.log(`error status : ${error.status} ${error.statusText}`);
									// console.log(error.error)
									switch (error.status) {
										case 400:
											this.spinner.hide();
											let errores = error.error.errors
											// console.log(error.error.errors)
											if (typeof errores.fileCvComercialArchivo !== 'undefined') {
												this.toastrService.danger('CV Comercial: El campo es requerido, solo se admite PDF', "Advertencia");
											}
											if (typeof errores.fileCvDocumentadoArchivo !== 'undefined') {
												this.toastrService.danger('CV Documentado: El campo es requerido, solo se admite PDF', "Advertencia");
											}
											if (typeof errores.fileFormatosArchivo !== 'undefined') {
												this.toastrService.danger('Formato: El campo es requerido, solo se admite PDF', "Advertencia");
											}
											break;
										case 401:      //login
											// this.router.navigateByUrl("/login");
											break;
										case 403:     //forbidden
											// this.router.navigateByUrl("/unauthorized");
											break;
									}
								}
							} else {
								console.error("some thing else happened");
							}
						});
					}else{
						this.spinner.hide();
					}
					return
				} else if (result.dismiss === Swal.DismissReason.cancel) {
					this.spinner.hide();
					// Swal.fire(
					// 	'Ca',
					// 	'Product still in our database.)',
					// 	'error'
					// )
				}
			})

		}else{
			this.toastrService.danger("Debe de ingresar los campos requeridos", "Sistema");
		}
		this.spinner.hide();

	}

	listaRegion()
	{
		this.postulante.regionPostulante().subscribe((data: any) => {
			// console.log(data)
			if (data.respuesta == 'OK') {
				data.datos.forEach((list: any) => {
					this.listRegion.push({
						codigo		: list.codDep,
						descripcion	: list.txtDepartamento
					})
				});
			} else {
				this.toastrService.danger("Ocurrio un error: " + data.mensaje, "POSTULANTE");
			}
			this.spinner.hide();
		},
		(error) => {
			// console.log(error)
			if (error instanceof HttpErrorResponse) {
				if (error.error instanceof ErrorEvent) {
					console.error("Error Event");
				} else {
					console.log(`error status : ${error.status} ${error.statusText}`);
					// console.log(error.error)
					switch (error.status) {
						case 400:
							this.spinner.hide();
							let errores = error.error.errors
							// console.log(error.error.errors)
							if (typeof errores.fileCvComercialArchivo !== 'undefined') {
								this.toastrService.danger('CV Comercial: El campo es requerido, solo se admite PDF', "Advertencia");
							}
							if (typeof errores.fileCvDocumentadoArchivo !== 'undefined') {
								this.toastrService.danger('CV Documentado: El campo es requerido, solo se admite PDF', "Advertencia");
							}
							if (typeof errores.fileFormatosArchivo !== 'undefined') {
								this.toastrService.danger('Formato: El campo es requerido, solo se admite PDF', "Advertencia");
							}
							break;
						case 401:      //login
							// this.router.navigateByUrl("/login");
							break;
						case 403:     //forbidden
							// this.router.navigateByUrl("/unauthorized");
							break;
					}
				}
			} else {
				console.error("some thing else happened");
			}
		});
	}

	changeRegion( value )
	{
		this.spinner.show();
		this.postulante.regionProvincia(value).subscribe((data: any) => {
			console.log(data)
			if (data.respuesta == 'OK') {
				this.listProvincia = []
				data.datos.forEach((list: any) => {
					this.listProvincia.push({
						codigo		: list.codProv,
						descripcion	: list.txtProvincia
					})
				});
			} else {
				this.toastrService.danger("Ocurrio un error: " + data.mensaje, "POSTULANTE");
			}
			this.spinner.hide();
		},
		(error) => {
			// console.log(error)
			if (error instanceof HttpErrorResponse) {
				if (error.error instanceof ErrorEvent) {
					console.error("Error Event");
				} else {
					console.log(`error status : ${error.status} ${error.statusText}`);
					// console.log(error.error)
					switch (error.status) {
						case 400:
							this.spinner.hide();
							let errores = error.error.errors
							// console.log(error.error.errors)
							if (typeof errores.fileCvComercialArchivo !== 'undefined') {
								this.toastrService.danger('CV Comercial: El campo es requerido, solo se admite PDF', "Advertencia");
							}
							if (typeof errores.fileCvDocumentadoArchivo !== 'undefined') {
								this.toastrService.danger('CV Documentado: El campo es requerido, solo se admite PDF', "Advertencia");
							}
							if (typeof errores.fileFormatosArchivo !== 'undefined') {
								this.toastrService.danger('Formato: El campo es requerido, solo se admite PDF', "Advertencia");
							}
							break;
						case 401:      //login
							// this.router.navigateByUrl("/login");
							break;
						case 403:     //forbidden
							// this.router.navigateByUrl("/unauthorized");
							break;
					}
				}
			} else {
				console.error("some thing else happened");
			}
		});
	}

	changeProvincia( value )
	{
		this.spinner.show();
		this.postulante.regionDistrito(value).subscribe((data: any) => {
			console.log(data)
			if (data.respuesta == 'OK') {
				this.listDistrito = []
				data.datos.forEach((list: any) => {
					this.listDistrito.push({
						codigo		: list.codUbigeo,
						descripcion	: list.txtDistrito
					})
				});
			} else {
				this.toastrService.danger("Ocurrio un error: " + data.mensaje, "POSTULANTE");
			}
			this.spinner.hide();
		},
		(error) => {
			// console.log(error)
			if (error instanceof HttpErrorResponse) {
				if (error.error instanceof ErrorEvent) {
					console.error("Error Event");
				} else {
					console.log(`error status : ${error.status} ${error.statusText}`);
					// console.log(error.error)
					switch (error.status) {
						case 400:
							this.spinner.hide();
							let errores = error.error.errors
							// console.log(error.error.errors)
							if (typeof errores.fileCvComercialArchivo !== 'undefined') {
								this.toastrService.danger('CV Comercial: El campo es requerido, solo se admite PDF', "Advertencia");
							}
							if (typeof errores.fileCvDocumentadoArchivo !== 'undefined') {
								this.toastrService.danger('CV Documentado: El campo es requerido, solo se admite PDF', "Advertencia");
							}
							if (typeof errores.fileFormatosArchivo !== 'undefined') {
								this.toastrService.danger('Formato: El campo es requerido, solo se admite PDF', "Advertencia");
							}
							break;
						case 401:      //login
							// this.router.navigateByUrl("/login");
							break;
						case 403:     //forbidden
							// this.router.navigateByUrl("/unauthorized");
							break;
					}
				}
			} else {
				console.error("some thing else happened");
			}
		});
	}

	

	onFileFormat(event) {
		// console.log(event.target.files[0])
		if (event.target.files.length > 0) {
			const file = event.target.files[0];
			let fotmato = this.formatBytes(file.size)
			let valida = this.validaPeso(fotmato.cantidad, fotmato.text, fotmato.formtat)
			if (valida) {
				this.filesData.formato = file
			}
		}
	}

	onFileComercial(event) {
		if (event.target.files.length > 0) {
			const file = event.target.files[0];
			let fotmato = this.formatBytes(file.size)
			let valida = this.validaPeso(fotmato.cantidad, fotmato.text, fotmato.formtat)
			if (valida) {
				this.filesData.comercial = file
			}
		}
	}

	onFileDocumentado(event) {
		if (event.target.files.length > 0) {
			const file = event.target.files[0];
			let fotmato = this.formatBytes(file.size)
			let valida = this.validaPeso(fotmato.cantidad, fotmato.text, fotmato.formtat)
			if (valida) {
				this.filesData.documentado = file
			}
		}
	}

	volver() {
		this.router.navigate(['convocatorias/publicas']);
	}

	validaNumeroci() {
		$('.numerico').keypress(function (tecla) {
			var reg = /^[0-9.\s]+$/;
			if (!reg.test(String.fromCharCode(tecla.which))) {
				return false;
			}
			return true;
		});
		$('.texto').keypress(function (tecla) {
			var reg = /^[a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ'_\s]+$/;
			if (!reg.test(String.fromCharCode(tecla.which))) {
				return false;
			}
			return true;
		});

	}

	buscarDni() {
		// console.log(e.target.value)
		// let numero = e.target.value
		let numero = this.grupoFiltro.controls['nro_doc'].value
		console.log(numero)
		if (numero.length >= 8) {
			this.spinner.show();
			this.postulante.consultaDNI({ 'dni': numero }).subscribe((data: any) => {
				console.log(data)
				if (data.ok) {
					this.spinner.hide();
					let ardata = JSON.parse(data.message)
					// console.log(ardata)
					this.grupoFiltro.controls['nombres'].setValue(ardata.prenombres)
					this.grupoFiltro.controls['apellidos'].setValue(ardata.apPrimer + ' ' + ardata.apSegundo)
					// this.grupoFiltro.controls['direccion'].setValue(ardata.direccion)
					// this.toastrService.success("Los archivos fueron enviados correctamente" ,"DOCUMENTO");
					// this.router.navigate(['convocatorias/publicas']);
				} else {
					this.toastrService.danger("El servicio no se encuentra activo", "POSTULANTE");
					// this.toastrService.danger("Ocurrio un error: " +data.message ,"POSTULANTE");
				}
				this.spinner.hide();
			},
				(error) => {
					// console.log(error)
					if (error instanceof HttpErrorResponse) {
						if (error.error instanceof ErrorEvent) {
							console.error("Error Event");
						} else {
							console.log(`error status : ${error.status} ${error.statusText}`);
							// console.log(error.error)
							switch (error.status) {
								case 400:
									this.spinner.hide();
									let errores = error.error.errors
									// console.log(error.error.errors)
									if (typeof errores.fileCvComercialArchivo !== 'undefined') {
										this.toastrService.danger('CV Comercial: ' + errores.fileCvComercialArchivo + ', solo se admite PDF', "Advertencia");
									}
									if (typeof errores.fileCvDocumentadoArchivo !== 'undefined') {
										this.toastrService.danger('CV Documentado: ' + errores.fileCvDocumentadoArchivo + ', solo se admite PDF', "Advertencia");
									}
									if (typeof errores.fileFormatosArchivo !== 'undefined') {
										this.toastrService.danger('Formato: ' + errores.fileFormatosArchivo + ', solo se admite PDF', "Advertencia");
									}
									break;
								case 401:      //login
									// this.router.navigateByUrl("/login");
									break;
								case 403:     //forbidden
									// this.router.navigateByUrl("/unauthorized");
									break;
							}
						}
					} else {
						console.error("some thing else happened");
					}
				});
		}
	}

	formatBytes(bytes) {
		var marker = 1024; // Change to 1000 if required
		var decimal = 3; // Change as required
		var kiloBytes = marker; // One Kilobyte is 1024 bytes
		var megaBytes = marker * marker; // One MB is 1024 KB
		var gigaBytes = marker * marker * marker; // One GB is 1024 MB
		var teraBytes = marker * marker * marker * marker; // One TB is 1024 GB

		// return bytes if less than a KB
		if (bytes < kiloBytes) return { text: bytes + " Bytes", cantidad: bytes, formtat: "Bytes" };
		// return KB if less than a MB
		else if (bytes < megaBytes) return { text: (bytes / kiloBytes).toFixed(decimal) + " KB", cantidad: (bytes / kiloBytes).toFixed(decimal), formtat: "KB" };
		// return MB if less than a GB
		else if (bytes < gigaBytes) return { text: (bytes / megaBytes).toFixed(decimal) + " MB", cantidad: (bytes / megaBytes).toFixed(decimal), formtat: "MB" };
		// return GB if less than a TB
		else return { text: (bytes / gigaBytes).toFixed(decimal) + " GB", cantidad: (bytes / gigaBytes).toFixed(decimal), formtat: "GB" };
	}

	validaPeso(peso = 0, cantidad = '', formato = '') {
		let validaFile = false
		if (formato == 'MB' || formato == 'GB') {
			if (peso > this.defaultPeso) {
				console.log('1')
				validaFile = true
			}
		}
		if (validaFile) {
			Swal.fire(
				'CONVOCATORIA!',
				'El archivo es de: ' + cantidad + ' y se requiere un peso máximo de ' + this.defaultPeso + ' MB'
			)
			return
		}
		return true
	}

	validaExisteFile() {
		let valida = true
		if (this.filesData.formato == '') {
			Swal.fire(
				'CONVOCATORIA!',
				'Se debe de ingresar el Formato',
				'error'
			)
			valida = false
		}
		if (this.filesData.comercial == '') {
			Swal.fire(
				'CONVOCATORIA!',
				'Se debe de ingresar el CV Comercial',
				'error'
			)
			valida = false
		}
		if (this.filesData.documentado == '') {
			Swal.fire(
				'CONVOCATORIA!',
				'Se debe de ingresar el CV Documentado',
				'error'
			)
			valida = false
		}
		return valida


	}
}
