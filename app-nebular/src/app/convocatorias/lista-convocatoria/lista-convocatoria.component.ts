import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NbToastrService } from '@nebular/theme';
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from '@angular/router';
import { ConvocatoriaService } from 'src/app/service/convocatoria.service';
import { DatePipe } from '@angular/common'
import { environment } from 'src/environments/environment';
// listaConvocatoria

@Component({
	selector: 'app-lista-convocatoria',
	templateUrl: './lista-convocatoria.component.html',
	styleUrls: ['./lista-convocatoria.component.scss']
})
export class ListaConvocatoriaComponent implements OnInit {

	grupoFiltro: FormGroup;
	submitted = false;
	listConvocatorias = []
	listEstado = []
	selectedItem

	showPagination: boolean;
	previousPage: number;
	totalItems: number;
	page: number;

	totalCarga: number = 0

	dtOptions: DataTables.Settings = {};

	urlDescarga = environment.API + 'api/convocatoria/descarga-archivo/';

	constructor(
		private formBuilder: FormBuilder,
		private toastrService: NbToastrService,
		private spinner: NgxSpinnerService,
		private router: Router,
		private convocatoria: ConvocatoriaService,
		public datepipe: DatePipe
	) {

	}

	ngOnInit(): void {

		this.validFormulario()

		this.loadDataConvocaria()

	}

	async loadDataConvocaria() {
		this.spinner.show()

		await this.convocatoria.listaConvocatoriaPublica().subscribe((data: any) => {
			console.log(data)
			if (data.respuesta = 'OK') {
				data.datos.forEach((list: any) => {
					this.listConvocatorias.push({
						codigo: list.codConvocatoria,
						descripcion: list.txtDescripcion,
						aviso: list.txtAvisoArchivo,
						pdf: list.txtTdrArchivo,
						formato: list.txtFormatosArchivo,
						fechaInicio: list.fecFechaInicio,
						fechaFin: list.fecFechaFin,
						estado: list.txtDescripcionTrazabilidad
					})
				});
				this.dataTable()
			}
			// this.totalCarga++
			this.spinner.hide();
		},
			(error) => {
				console.log(error)
			});

		this.dataFalseEstados().forEach((list: any) => {
			this.listEstado.push({
				codigo: list.codigo,
				descripcion: list.descripcion
			})
		});

	}

	validFormulario() {
		this.grupoFiltro = this.formBuilder.group({
			descripcion: [''],
			estado: ['']
		});
	}
	get f() { return this.grupoFiltro.controls; }

	filtrarRegistros() {
		this.spinner.show();
		this.submitted = true;
		if (!this.grupoFiltro.invalid) {
			let conv = this.grupoFiltro.controls['descripcion'].value
			console.log(conv)
			$('#tablaLists').DataTable().column(1).search(conv).draw();

			let estado = this.grupoFiltro.controls['estado'].value
			$('#tablaLists').DataTable().column(7).search(estado).draw();
			this.spinner.hide();
			return
		}
		setTimeout(() => {
			this.spinner.hide();
			this.toastrService.danger("Ocurrio un error", "Archivo");
		}, 2000);
	}


	dataTable() {
		$(function () {
			$('#tablaLists').DataTable({
				ordering: false,
				language: {

					emptyTable: '',
					zeroRecords: 'No hay coincidencias',
					lengthMenu: 'Mostrar _MENU_ elementos',
					search: 'Buscar:',
					info: 'De _START_ a _END_ de _TOTAL_ elementos',
					infoEmpty: 'De 0 a 0 de 0 elementos',
					infoFiltered: '(filtrados de _MAX_ elementos totales)',
					paginate: {
						first: 'Prim.',
						last: 'Últ.',
						next: 'Sig.',
						previous: 'Ant.'
					},
				},
				"order": [[6, "desc"]]
			});
		});
	}


	verDocumento(data) {

		// console.log(this.urlDescarga+data)
		this.toastrService.warning("Ver documento : " + data, "Archivo PDF");
		window.open(this.urlDescarga + data, '_blank');

	}

	dataFalseConvocatoria() {
		const dataJson = [
			{
				codConvocatoria: 14,
				txtDescripcion: "Servicio de Consultoría para la Preparación y Ejecución de la Línea de Base del Segundo Componente en el Marco de la Ejecución del PIADER xxxx",
				txtAvisoArchivo: "demo1.pdf",
				txtTdrArchivo: "demo1.pdf",
				txtFormatosArchivo: "demo1.pdf",
				fecFechaInicio: "11-05-2021",
				fecFechaFin: "15-05-2021",
				intEstadoTrazabilidad: 2,
				txtDescripcionTrazabilidad: "Vigente",
				intEstadoRegistro: 1,
				fecFechaRegistro: "2021-05-10T13:31:55"
			}
		]
		return dataJson
	}

	dataFalseEstados() {
		const dataJson = [
			{
				codigo: "VIGENTE",
				descripcion: "VIGENTE"
			},
			{
				codigo: "CONCLUIDO",
				descripcion: "CONCLUIDO"
			}
		]
		return dataJson
	}

	postularParticipante(data) {
		// console.log(data.codigo)
		// return
		this.router.navigate(['postular/' + data.codigo]);
	}
	// ----------------------------------
	fillStudents(page: number): void {
		this.showPagination = true
	}

	loadPage(page: number) {
		if (page !== this.previousPage) {
			this.previousPage = page;
			this.fillStudents(this.page - 1);
		}
	}

	filtrar() {

	}
}
