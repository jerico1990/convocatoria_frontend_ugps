import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConvocatoriasRoutingModule } from './convocatorias-routing.module';
import { RegistroPostulanteComponent } from './registro-postulante/registro-postulante.component';
import { ListaConvocatoriaComponent } from './lista-convocatoria/lista-convocatoria.component';

import { NbThemeModule, NbLayoutModule, NbSidebarModule, NbCardModule, NbButtonModule, NbActionsModule, NbIconModule, NbInputModule, NbFormFieldModule, NbStepperModule, NbAlertModule, NbDialogModule, NbBadgeModule, NbTooltipModule, NbCalendarRangeModule, NbCalendarModule, NbListModule, NbUserModule, NbTabsetModule, NbSpinnerModule, NbToastrModule, NbMenuModule, NbRadioModule, NbSelectModule, NbCheckboxModule, NbAccordionModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { NgxSpinnerModule } from "ngx-spinner";
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { NgxCaptchaModule } from 'ngx-captcha';
@NgModule({
	declarations: [RegistroPostulanteComponent, ListaConvocatoriaComponent],
	imports: [
		CommonModule,
		ConvocatoriasRoutingModule,
		NbThemeModule.forRoot({ name: 'corporate' }),
		NbLayoutModule,
		NbEvaIconsModule,
		// AppRoutingModule,
		NgxSpinnerModule,
		NbSidebarModule.forRoot(),
		NbCardModule,
		NbButtonModule,
		NbActionsModule,
		NbIconModule,
		NbInputModule,
		HttpClientModule,
		NbFormFieldModule,
		NbStepperModule,
		FormsModule,
		ReactiveFormsModule,
		NgxCaptchaModule,
		NbAlertModule,
		NbDialogModule.forRoot({
			closeOnBackdropClick : false
		}),
		NbBadgeModule,
		NbTooltipModule,
		NbCalendarRangeModule,
		NbCalendarModule,
		NbListModule,
		NbUserModule,
		NbTabsetModule,
		NbSpinnerModule,
		NbToastrModule.forRoot({
			duration: 5000,
		}),
		NbMenuModule.forRoot(),
		NbRadioModule,
		NbSelectModule,
		NbCheckboxModule,
		NbAccordionModule,

		SharedModule,
		DataTablesModule,
	],
	exports:[
		RegistroPostulanteComponent, 
		ListaConvocatoriaComponent
	]
})
export class ConvocatoriasModule { }
