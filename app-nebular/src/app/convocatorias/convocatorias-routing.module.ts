import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaConvocatoriaComponent } from './lista-convocatoria/lista-convocatoria.component';
import { RegistroPostulanteComponent } from './registro-postulante/registro-postulante.component';


const routes: Routes = [
  { path: 'convocatorias/publicas' , component: ListaConvocatoriaComponent},
  { path: 'postular/:id' , component: RegistroPostulanteComponent },
  { path: '**', redirectTo: 'convocatorias/publicas'},
  // {path: '**', redirectTo: '/404'}
  // { path: '**', redirectTo: '/login' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConvocatoriasRoutingModule { }
