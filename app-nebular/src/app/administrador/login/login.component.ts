import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from "ngx-spinner";
import { NbToastrService } from '@nebular/theme';
import { ConvocatoriaService } from 'src/app/service/convocatoria.service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
	grupoFiltro: FormGroup;
	submitted = false;
	siteKey: string = "6LeiOQQbAAAAAKq7YX84yGb5h3nz4PHco7pqX6lp"

	constructor(
		private formBuilder: FormBuilder,
		private spinner: NgxSpinnerService,
		private toastrService: NbToastrService,
		private router: Router,
		private convocatoria: ConvocatoriaService,
	) {
		
	}

	ngOnInit(): void {
		let usuario = sessionStorage.getItem('token')
		if (typeof usuario != 'undefined' && usuario != null) {
			window.location.href = 'appConvocatoria/convocatorias/interna'
		}
		this.validFormulario()
	}

	validFormulario() {
		this.grupoFiltro = this.formBuilder.group({
			usuario: ['', Validators.required],
			clave: ['', Validators.required],
			recaptcha: ['', Validators.required]
		});
	}
	get f() { return this.grupoFiltro.controls; }

	login() {
		this.spinner.show();
		this.submitted = true;
		if (!this.grupoFiltro.invalid) {
			let usuario = this.grupoFiltro.controls['usuario'].value
			let clave = this.grupoFiltro.controls['clave'].value

			const arrClave = {
				'usuario': usuario,
				'clave': clave,
			}
			this.convocatoria.login(arrClave).subscribe((data: any) => {
				// console.log(data)
				if (data.token !== '') {
					// sessionStorage.setItem('credenciales', JSON.stringify(data.data));
					sessionStorage.setItem('token', data.token);
					sessionStorage.setItem('usuario', data.txtUsuario);
					sessionStorage.setItem('codUsuario', data.codUsuario);
					// this.toastrService.success("Se registro correctamente la convocatoria" ,"CONVOCATORIA");
					// this.router.navigate(['convocatorias/interna']);
					window.location.href = '/appConvocatoria/convocatorias/interna'
				} else {
					this.toastrService.danger("Ocurrio un error: " + data.mensaje, "CONVOCATORIA");
				}
				this.spinner.hide();
			},
				error => {
					//console.log(error)
					if (error instanceof HttpErrorResponse) {
						if (error.error instanceof ErrorEvent) {
							//console.error("Error Event");
						} else {
							//console.log(`error status : ${error.status} ${error.statusText}`);
							// console.log(error.error)
							this.spinner.hide();
							switch (error.status) {
								case 400:
									this.toastrService.danger("Usuario y/o clave estan incorrectos", "CONVOCATORIA");
									break;
								case 401:      //login
									this.router.navigateByUrl("/");
									break;
								case 403:     //forbidden
									// this.router.navigateByUrl("/unauthorized");
									break;
							}
						}
					} else {
						// console.error("some thing else happened");
					}
				});

			// setTimeout(() => {
			// 	this.spinner.hide();
			// 	this.toastrService.success("Se envio los datos correctamente" ,"Archivo enviado");
			// }, 2000);
			return
		}
		// setTimeout(() => {
		this.spinner.hide();
		this.toastrService.danger("Debe de ingresar los datos", "CONVOCATORIA");
		// }, 2000);
	}
}
