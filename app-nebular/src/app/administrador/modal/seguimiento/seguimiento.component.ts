import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NbToastrService, NbMenuItem } from '@nebular/theme';
import { NgxSpinnerService } from "ngx-spinner";
import { Router, ActivatedRoute } from '@angular/router';
import { ConvocatoriaService } from 'src/app/service/convocatoria.service';
import { PostulanteService } from 'src/app/service/postulante.service';
import { environment } from 'src/environments/environment';
import { HttpErrorResponse } from '@angular/common/http';
@Component({
	selector: 'app-seguimiento',
	templateUrl: './seguimiento.component.html',
	styleUrls: ['./seguimiento.component.scss']
})
export class SeguimientoComponent implements OnInit {

	formularioConvocatoria: FormGroup;
	submitted = false;
	listPostulantes = []
	listEstado = []
	selectedItem
	codigo: any;
	datosConvocatoria = {
		nombre: "",
		codigo: "",
		correlativo: "",
		estado: "",
		fInicio: "",
		fFin: "",
		tdr: "",
		aviso: "",
		formato: "",
		trazabilidad: "",
		publicado: "",
		nroOs: "",
		osArchivo: "",
		cantidad : 0
	}
	totalPostulante: number = 0

	filesData: any = {
		orden: ''
	}
	urlDescarga = environment.API + 'api/convocatoria/descarga-archivo/';
	urlDescargaPostulante = environment.API + 'api/postulante/descarga-archivo/';
	guardarDato = ""
	finalizado = 0
	totalPostulanteAdjudicado = 0
	seguimientoConv = true
	seguimientoConvBtn = false
	constructor(
		private formBuilder: FormBuilder,
		private toastrService: NbToastrService,
		private spinner: NgxSpinnerService,
		private router: Router,
		private route: ActivatedRoute,
		private convocatoria: ConvocatoriaService,
		private postulante: PostulanteService,
	) { }

	ngOnInit(): void {
		let usuario = sessionStorage.getItem('token')
		if (typeof usuario !== 'undefined' && usuario == null) {
			// this.router.navigateByUrl("/");
			this.router.navigate(["/"]);
		}
		this.validFormulario()
		// this.loadDataConvocaria()

		this.codigo = this.route.snapshot.paramMap.get('id');
		this.loadData(this.codigo)
	}

	validaLoadSeguimiento()
	{
		if(this.datosConvocatoria.cantidad >= this.totalPostulanteAdjudicado)
		{
			this.seguimientoConv = false
		}

		if( (this.datosConvocatoria.cantidad === this.totalPostulanteAdjudicado) && this.totalPostulanteAdjudicado != 0 )
		{
			this.seguimientoConvBtn = true
		}
		
	}

	loadData(id) {
		this.spinner.show()
		this.convocatoria.listaConvocatoriaId({ id: id }).subscribe((data: any) => {
			// console.log(data)
			if (data.respuesta = 'OK') {
				this.datosConvocatoria.nombre = data.datos[0].txtDescripcion
				this.datosConvocatoria.codigo = data.datos[0].codConvocatoria
				this.datosConvocatoria.correlativo = data.datos[0].txtCorrelativo
				this.datosConvocatoria.estado = data.datos[0].txtDescripcionTrazabilidad
				this.datosConvocatoria.fInicio = data.datos[0].fecFechaInicio
				this.datosConvocatoria.fFin = data.datos[0].fecFechaFin

				this.datosConvocatoria.tdr = data.datos[0].txtTdrArchivo
				this.datosConvocatoria.aviso = data.datos[0].txtAvisoArchivo
				this.datosConvocatoria.formato = data.datos[0].txtFormatosArchivo

				this.datosConvocatoria.trazabilidad = data.datos[0].intEstadoTrazabilidad
				this.datosConvocatoria.publicado = data.datos[0].intPublicado

				this.datosConvocatoria.cantidad = data.datos[0].intCantidadAdjudicados

				// this.formularioConvocatoria.controls['ordenServicio'].setValue(data.datos[0].txtNroOs)

				if(this.datosConvocatoria.fFin !== null) {
					let fechaFinal = this.datosConvocatoria.fFin.split("-")
					this.formularioConvocatoria.controls['fechaFin'].setValue(fechaFinal[2] + '-' + fechaFinal[1] + "-" + fechaFinal[0])
				}

				// this.datosConvocatoria.nroOs = data.datos[0].txtNroOs
				this.datosConvocatoria.osArchivo = data.datos[0].txtOsArchivo

			}

			this.convocatoria.listaPostulante({
				id: data.datos[0].codConvocatoria
			}).subscribe((data: any) => {
				// console.log(data)
				if (data.respuesta = 'OK') {
					this.totalPostulante = data.datos.length
					data.datos.forEach((list: any) => {
						this.listPostulantes.push({
							codigo: list.codPostulante,
							postulante: list.txtApellidos + ',' + list.txtNombres,
							celular: list.txtCelular,
							correo: list.txtCorreoElectronico,
							direccion: list.txtDireccion,
							nroDocumento: list.txtNroDocumento,
							formato: list.txtFormatosArchivo,
							comercial: list.txtCvComercialArchivo,
							documentado: list.txtCvComercialArchivo,
							adjudicado: list.intEstadoTrazabilidad,
						});

						if (list.intEstadoTrazabilidad == 2) {
							this.totalPostulanteAdjudicado++;
						}
					});
					this.dataTable()
					this.validaLoadSeguimiento()
				}
			},
				(error) => {
					console.log(error)
				});

			this.spinner.hide();
		},
		(error) => {
			// console.log(error)
			if (error instanceof HttpErrorResponse) {
				if (error.error instanceof ErrorEvent) {
					console.error("Error Event");
				} else {
					console.log(`error status : ${error.status} ${error.statusText}`);
					// console.log(error.error)
					this.spinner.hide();
					switch (error.status) {
						case 400:
							console.log(error)
							break;
						case 401:      //login
							this.router.navigateByUrl("/");
							break;
						case 403:     //forbidden
							// this.router.navigateByUrl("/unauthorized");
							break;
					}
				}
			} else {
				// console.error("some thing else happened");
			}
		});
	}


	adjudicarConvocatoria() {

		if (this.guardarDato == '3' && this.totalPostulanteAdjudicado == 0) {
			this.toastrService.danger("Se debe adjudicar a un postulante", "Convocatoria");
			return false;
		}

		this.spinner.show();
		this.submitted = true;

		if (!this.formularioConvocatoria.invalid) {
			let codConvocatoria = this.codigo
			// let ordenServicio = this.formularioConvocatoria.controls['ordenServicio'].value
			let fechaFin = this.formularioConvocatoria.controls['fechaFin'].value
			let fechaFormat = ''
			if (fechaFin != '') {
				let ff = fechaFin.split('-')
				fechaFormat = ff[2] + '-' + ff[1] + '-' + ff[0]
			}
			const formData = new FormData()
			formData.append('codConvocatoria', codConvocatoria)
			formData.append('txtNroOs', "")
			formData.append('fileOsArchivo', "")
			formData.append('intEstadoTrazabilidad', this.guardarDato)
			formData.append('intPublicado', this.datosConvocatoria.publicado)
			formData.append('fecFechaFin', fechaFormat)
			// console.log(formData)
			// return
			this.postulante.adjudicacionConvocatoria(formData).subscribe((data: any) => {
				if (data.respuesta == 'OK') {

					if (this.guardarDato == '2') {
						window.location.reload()
						// this.router.navigate(['convocatorias/interna']);
						this.toastrService.success("Se guardo correctamente los datos", "CONVOCATORIA");
					} else {
						// this.formularioConvocatoria.controls['ordenServicio'].disable();
						this.finalizado = 1
						// this.formularioConvocatoria.controls['fileServicio'].disable();
						this.formularioConvocatoria.controls['fechaFin'].disable();
						this.toastrService.success("Se finalizo correctamente la información", "CONVOCATORIA");
					}
				} else {
					this.toastrService.danger("Ocurrio un error: " + data.mensaje, "POSTULANTE");
				}
				this.spinner.hide();
			},
				(error) => {
					console.log(error)
				});
			this.spinner.hide();
			return
		}
		// setTimeout(() => {
		// }, 2000);
		this.spinner.hide();
		this.toastrService.danger("Se debe de ingresar los datos requeridos", "Convocatoria");
	}

	validFormulario() {
		this.formularioConvocatoria = this.formBuilder.group({
			//fileServicio: ['', Validators.required],
			// ordenServicio: ['', Validators.required],
			fechaFin: ['']
		});

	}
	get f() { return this.formularioConvocatoria.controls; }

	verPostulante(data) {
		window.open(this.urlDescargaPostulante + data, '_blank');
		this.toastrService.warning("Ver documento : " + data, "Archivo PDF");
	}

	verDocumento(data) {
		window.open(this.urlDescarga + data, '_blank');
		this.toastrService.warning("Ver documento : " + data, "Archivo PDF");
	}


	volverConvocatoria($myParam: string = ''): void {
		const navigationDetails: string[] = ['/convocatorias/interna'];
		if ($myParam.length) {
			navigationDetails.push($myParam);
		}
		this.router.navigate(navigationDetails);
	}

	onFileOrdenServicio(event) {
		if (event.target.files.length > 0) {
			const file = event.target.files[0];
			this.filesData.orden = file
		}
	}


	datoGuardar(data) {
		this.guardarDato = data
	}


	adjudicaPostulante(codPostulante) {
		this.spinner.show();
		let codConvocatoria = this.codigo

		const adjudicacion = {
			"codConvocatoria": parseFloat(codConvocatoria),
			"codPostulante": codPostulante
		}

		this.listPostulantes.forEach(element => {
			if (element.codigo == codPostulante) {
				element.adjudicado = 1
			} else {
				element.adjudicado = 2
			}
		});
		// return
		this.postulante.adjudicacionPostulante(adjudicacion).subscribe((data: any) => {
			if (data.respuesta == 'OK') {
				this.toastrService.success("Se adjudico correctamente al postulante", "POSTULANTE");
				// this.router.navigate(['convocatorias/publicas']);
			} else {
				this.toastrService.danger("Ocurrio un error: " + data.mensaje, "POSTULANTE");
			}
			this.spinner.hide();
			window.location.reload()
		},
			(error) => {
				console.log(error)
			});
		// this.spinner.hide();
	}


	quitarDocumento() {
		this.datosConvocatoria.osArchivo = '';
	}

	dataTable() {

		$(function () {
			var confing = {
				ordering: false,
				dom: 'Bfrtip',
				buttons: [
					{
						text:   'Excel',
						extend: 'excel',
						filename: function(){
							var d = new Date();
							var n = d.getTime();
							return 'postulantes_' + n;
						},
					}
				],
				language: {
					emptyTable: '',
					zeroRecords: 'No hay coincidencias',
					lengthMenu: 'Mostrar _MENU_ elementos',
					search: 'Buscar:',
					info: 'De _START_ a _END_ de _TOTAL_ elementos',
					infoEmpty: 'De 0 a 0 de 0 elementos',
					infoFiltered: '(filtrados de _MAX_ elementos totales)',
					paginate: {
						first: 'Prim.',
						last: 'Últ.',
						next: 'Sig.',
						previous: 'Ant.'
					},
				},
			}
			$('#tabla_postulantes').DataTable(confing);

		});
	}

}
