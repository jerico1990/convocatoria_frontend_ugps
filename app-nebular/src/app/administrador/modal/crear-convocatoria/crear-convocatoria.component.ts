import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NbToastrService } from '@nebular/theme';
import { NgxSpinnerService } from "ngx-spinner";
import { Router, ActivatedRoute } from '@angular/router';
import { ConvocatoriaService } from 'src/app/service/convocatoria.service';
import { HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Component({
	selector: 'app-crear-convocatoria',
	templateUrl: './crear-convocatoria.component.html',
	styleUrls: ['./crear-convocatoria.component.scss']
})
export class CrearConvocatoriaComponent implements OnInit {

	formularioConvocatoria: FormGroup;
	submitted = false;
	listConvocatorias = []
	listEstado = []
	listTrazabilidad = []
	selectedItem;
	codigoConvocatoria: any
	filesData: any = {
		formato: '',
		valFormat: "0",
		tdr: '',
		valTdr: "0",
		aviso: '',
		valAviso: "0",
	}
	urlDescarga = environment.API + 'api/convocatoria/descarga-archivo/';

	constructor(
		private formBuilder: FormBuilder,
		private toastrService: NbToastrService,
		private spinner: NgxSpinnerService,
		private router: Router,
		private convocatoria: ConvocatoriaService,
		private route: ActivatedRoute,
	) { }

	ngOnInit(): void {
		let usuario = sessionStorage.getItem('token')
		if (typeof usuario !== 'undefined' && usuario == null) {
			// this.router.navigateByUrl("/");
			this.router.navigate(["/"]);
		}
		this.validFormulario()
		this.codigoConvocatoria = this.route.snapshot.paramMap.get('id');
		// console.log(this.codigoConvocatoria)
		if (this.codigoConvocatoria) {
			this.loadConvocatoriaId(this.codigoConvocatoria)
			this.trazabilidad()
		}
		this.loadEstado()
	}

	loadConvocatoriaId(codigo: Number) {
		this.convocatoria.listaConvocatoriaId({
			id: codigo
		}).subscribe((data: any) => {
			if (data.respuesta == 'OK') {
				let datos 		= data.datos[0]
				let fechaIni 	= datos.fecFechaInicio.split("-")
				let fechaFinal 	= datos.fecFechaFin.split("-")

				this.formularioConvocatoria.controls['nroConvocatoria'].setValue(datos.txtCorrelativo)
				this.formularioConvocatoria.controls['descripcion'].setValue(datos.txtDescripcion)
				this.formularioConvocatoria.controls['estado'].setValue(datos.intPublicado)
				this.formularioConvocatoria.controls['cantidad'].setValue(datos.intCantidadAdjudicados)
				
				this.formularioConvocatoria.controls['fInicio'].setValue(fechaIni[2] + '-' + fechaIni[1] + "-" + fechaIni[0])
				this.formularioConvocatoria.controls['fFin'].setValue(fechaFinal[2] + '-' + fechaFinal[1] + "-" + fechaFinal[0])

				this.filesData.tdr 		= datos.txtTdrArchivo
				this.filesData.aviso 	= datos.txtAvisoArchivo
				this.filesData.formato 	= datos.txtFormatosArchivo

				this.filesData.valFormat 	= 1
				this.filesData.valTdr 		= 1
				this.filesData.valAviso 	= 1

				this.formularioConvocatoria.patchValue({
					trazabilidad: datos.intEstadoTrazabilidad,
				});
			}
			this.spinner.hide();
		},
		(error) => {
			console.log(error)
		});
	}

	trazabilidad() {
		this.convocatoria.listaConvocatoriaTrazabilidad().subscribe((data: any) => {
			if (data.respuesta == 'OK') {
				// console.log(data)
				// return
				// let datos = data.datos[0]
				data.datos.forEach((list: any) => {
					this.listTrazabilidad.push({
						codigo: list.intCodigo,
						descripcion: list.txtDescripcion
					})
				})
				// console.log(this.listTrazabilidad)
			}
			this.spinner.hide();
		},
			(error) => {
				console.log(error)
			});
	}

	registroConvocatoria() {
		this.spinner.show();
		this.submitted = true;
		if (!this.formularioConvocatoria.invalid) {
			let descripcion = this.formularioConvocatoria.controls['descripcion'].value
			let fInicio = this.formularioConvocatoria.controls['fInicio'].value
			let fFin = this.formularioConvocatoria.controls['fFin'].value
			let estado = this.formularioConvocatoria.controls['estado'].value
			let codUsuario = sessionStorage.getItem('codUsuario')
			let cantidad = this.formularioConvocatoria.controls['cantidad'].value
			let fechaInicio = fInicio.split('-')
			let fechaFin = fFin.split('-')
			const formData = new FormData()
			formData.append('txtDescripcion', descripcion)
			formData.append('fecFechaInicio', fechaInicio[2] + '-' + fechaInicio[1] + '-' + fechaInicio[0])
			formData.append('fecFechaFin', fechaFin[2] + '-' + fechaFin[1] + '-' + fechaFin[0])
			formData.append('intPublicado', estado)
			formData.append('intCantidadAdjudicados', cantidad)
			formData.append('intUsuario', codUsuario)

			formData.append('fileAvisoArchivo', this.filesData.aviso)
			if (this.filesData.valAviso != '0') {
			}
			formData.append('fileTdrArchivo', this.filesData.tdr)
			if (this.filesData.valTdr != '0') {
			}
			formData.append('fileFormatosArchivo', this.filesData.formato)
			if (this.filesData.valAviso != '0') {
			}

			if (this.codigoConvocatoria == null) {
				this.convocatoria.crearConvocatoria(formData).subscribe((data: any) => {
					console.log(data)
					if (data.respuesta == 'OK') {
						this.toastrService.success("Se registro correctamente la convocatoria", "CONVOCATORIA");
						this.router.navigate(['convocatorias/interna']);
					} else {
						this.toastrService.danger("Ocurrio un error: " + data.mensaje, "CONVOCATORIA");
					}
					this.spinner.hide();
				},
				error => {
					if (error instanceof HttpErrorResponse) {
						if (error.error instanceof ErrorEvent) {
							console.error("Error Event");
						} else {
							console.log(`error status : ${error.status} ${error.statusText}`);
							// console.log(error.error)
							switch (error.status) {
								case 400:
									this.spinner.hide();
									let errores = error.error.errors
									// console.log(error.error.errors)
									if (typeof errores.fileTdrArchivo !== 'undefined') {
										this.toastrService.danger('TDR: El campo es requerido, solo se admite PDF', "Advertencia");
										console.log(errores.fileTdrArchivo);
									}
									if (typeof errores.fileAvisoArchivo !== 'undefined') {
										this.toastrService.danger('AVISO: El campo es requerido, solo se admite PDF', "Advertencia");
										console.log(errores.fileAvisoArchivo);
									}
									if (typeof errores.fileFormatosArchivo !== 'undefined') {
										this.toastrService.danger('Formato: El campo es requerido, solo se admite WORD', "Advertencia");
										console.log(errores.fileFormatosArchivo);
									}
									break;
								case 401:      //login
									this.router.navigateByUrl("/");
									break;
								case 403:     //forbidden
									// this.router.navigateByUrl("/unauthorized");
									break;
							}
						}
					} else {
						console.error("some thing else happened");
					}
				});
			} else {
				let trazabildad = this.formularioConvocatoria.controls['trazabilidad'].value
				let cantidad = this.formularioConvocatoria.controls['cantidad'].value

				formData.append('codConvocatoria', this.codigoConvocatoria)
				formData.append('intEstadoTrazabilidad', trazabildad )
				formData.append('intCantidadAdjudicados', cantidad)
				this.convocatoria.updateConvocatoria(formData).subscribe((data: any) => {
					console.log(data)
					if (data.respuesta == 'OK') {
						if ( estado == 1 && trazabildad == 2){
							this.router.navigate(['seguimiento-convocatoria/'+this.codigoConvocatoria]);	
						}
						this.toastrService.success("Se actualizo correctamente la convocatoria", "CONVOCATORIA");
						//this.router.navigate(['convocatorias/interna']);
					} else {
						this.toastrService.danger("Ocurrio un error: " + data.mensaje, "CONVOCATORIA");
					}
					this.spinner.hide();
				},
				error => {
					// console.log(error)
					if (error instanceof HttpErrorResponse) {
						if (error.error instanceof ErrorEvent) {
							console.error("Error Event");
						} else {
							console.log(`error status : ${error.status} ${error.statusText}`);
							// console.log(error.error)
							switch (error.status) {
								case 400:
									this.spinner.hide();
									let errores = error.error.errors
									// console.log(error.error.errors)
									if (typeof errores.fileTdrArchivo !== 'undefined') {
										this.toastrService.danger('TDR: El campo es requerido, solo se admite PDF', "Advertencia");
										console.log(errores.fileTdrArchivo);
									}
									if (typeof errores.fileAvisoArchivo !== 'undefined') {
										this.toastrService.danger('AVISO: El campo es requerido, solo se admite PDF', "Advertencia");
										console.log(errores.fileAvisoArchivo);
									}
									if (typeof errores.fileFormatosArchivo !== 'undefined') {
										this.toastrService.danger('Formato: El campo es requerido, solo se admite WORD', "Advertencia");
										console.log(errores.fileFormatosArchivo);
									}
									break;
								case 401:      //login
									this.router.navigateByUrl("/");
									break;
								case 403:     //forbidden
									// this.router.navigateByUrl("/unauthorized");
									break;
							}
						}
					} else {
						console.error("some thing else happened");
					}
				});
			}
			return
		}
		this.toastrService.danger("Debe de ingresar los campos requeridos", "Sistema");
		this.spinner.hide();
	}

	validFormulario() {
		this.formularioConvocatoria = this.formBuilder.group({
			// nroConvocatoria			: ['', Validators.required],
			descripcion: ['', Validators.required],
			fInicio: ['', Validators.required],
			fFin: ['', Validators.required],
			estado: ['', Validators.required],
			cantidad: ['', Validators.required],
			tdr: [''],
			aviso: [''],
			formato: [''],
			trazabilidad: [''],
			nroConvocatoria: [''],
		});
	}
	get f() { return this.formularioConvocatoria.controls; }

	volverConvocatoria($myParam: string = ''): void {
		const navigationDetails: string[] = ['/convocatorias/interna'];
		if ($myParam.length) {
			navigationDetails.push($myParam);
		}
		this.router.navigate(navigationDetails);
	}

	onFileFormat(event) {
		// console.log(event.target.files[0])
		if (event.target.files.length > 0) {
			const file = event.target.files[0];
			this.filesData.formato = file
			this.filesData.valFormat = 0
		}
	}

	onFileTdr(event) {
		if (event.target.files.length > 0) {
			const file = event.target.files[0];
			this.filesData.tdr = file
			this.filesData.valTdr = 0
		}
	}

	onFileAviso(event) {
		if (event.target.files.length > 0) {
			const file = event.target.files[0];
			this.filesData.aviso = file
			this.filesData.valAviso = 0
		}
	}

	eliminarArchivo(msg) {
		switch (msg) {
			case 'aviso':
				this.filesData.aviso = ''
				break;
			case 'tdr':
				this.filesData.tdr = ''
				break;
			case 'formato':
				this.filesData.formato = ''
				break;
		}
	}

	verDocumento(data) {
		// this.urlDescarga+data
		switch (data) {
			case 'aviso':
				window.open(this.urlDescarga + this.filesData.aviso, '_blank');
				break;
			case 'tdr':
				window.open(this.urlDescarga + this.filesData.tdr, '_blank');
				break;
			case 'formato':
				window.open(this.urlDescarga + this.filesData.formato, '_blank');
				break;
		}

	}

	loadEstado()
	{
		const estado = [
			{
				codigo : "0",
				descripcion : 'NO PUBLICADO'
			},
			{
				codigo : "1",
				descripcion : 'PUBLICADO'
			}
		]
		// console.log(estado)
		estado.forEach((list: any) => {
			console.log(list.codigo)
			this.listEstado.push({
				codigo: list.codigo,
				descripcion: list.descripcion
			})
		})
	}

}
