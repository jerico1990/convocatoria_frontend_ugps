import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaPostulantesComponent } from './lista-postulantes/lista-postulantes.component';
import { LoginComponent } from './login/login.component';
import { CrearConvocatoriaComponent } from './modal/crear-convocatoria/crear-convocatoria.component';
import { SeguimientoComponent } from './modal/seguimiento/seguimiento.component';


const routes: Routes = [
	// { path: '', pathMatch: 'full', redirectTo: 'login' },
	// { path: '*' , component: LoginComponent, pathMatch: 'full' },
	{ path: 'convocatorias/interna' , component: ListaPostulantesComponent},
	{ path: 'crear-convocatoria' , component: CrearConvocatoriaComponent},
	{ path: 'editar-convocatoria/:id' , component: CrearConvocatoriaComponent},
	{ path: 'seguimiento-convocatoria/:id' , component: SeguimientoComponent},
	{ path: 'login' , component: LoginComponent},
	// { path: '**', redirectTo: 'login'},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class AdministradorRoutingModule { }
