import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdministradorRoutingModule } from './administrador-routing.module';
import { ListaPostulantesComponent } from './lista-postulantes/lista-postulantes.component';
import { LoginComponent } from './login/login.component';
import { CrearConvocatoriaComponent } from './modal/crear-convocatoria/crear-convocatoria.component';
import { SeguimientoComponent } from './modal/seguimiento/seguimiento.component';

import { NbThemeModule, NbLayoutModule, NbSidebarModule, NbCardModule, NbButtonModule, NbActionsModule, NbIconModule, NbInputModule, NbFormFieldModule, NbStepperModule, NbAlertModule, NbDialogModule, NbBadgeModule, NbTooltipModule, NbCalendarRangeModule, NbCalendarModule, NbListModule, NbUserModule, NbTabsetModule, NbSpinnerModule, NbToastrModule, NbMenuModule, NbRadioModule, NbSelectModule, NbCheckboxModule, NbAccordionModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { NgxSpinnerModule } from "ngx-spinner";
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';

import { BrowserModule } from '@angular/platform-browser';
import { DataTablesModule } from 'angular-datatables';
import { NgxCaptchaModule } from 'ngx-captcha';

@NgModule({
	declarations: [ListaPostulantesComponent, LoginComponent, CrearConvocatoriaComponent, SeguimientoComponent],
	imports: [
		CommonModule,
		AdministradorRoutingModule,
		NbThemeModule.forRoot({ name: 'corporate' }),
		NbLayoutModule,
		NbEvaIconsModule,
		// AppRoutingModule,
		NgxSpinnerModule,
		NbSidebarModule.forRoot(),
		NbCardModule,
		NbButtonModule,
		NbActionsModule,
		NbIconModule,
		NbInputModule,
		HttpClientModule,
		NbFormFieldModule,
		NbStepperModule,
		FormsModule,
		ReactiveFormsModule,
		NgxCaptchaModule,
		NbAlertModule,
		NbDialogModule.forRoot({
			closeOnBackdropClick : false
		}),
		NbBadgeModule,
		NbTooltipModule,
		NbCalendarRangeModule,
		NbCalendarModule,
		NbListModule,
		NbUserModule,
		NbTabsetModule,
		NbSpinnerModule,
		NbToastrModule.forRoot({
			duration: 5000,
		}),
		NbMenuModule.forRoot(),
		NbRadioModule,
		NbSelectModule,
		NbCheckboxModule,
		NbAccordionModule,
		SharedModule,

		BrowserModule,
		DataTablesModule,
	],
	exports:[
		ListaPostulantesComponent, 
		LoginComponent, 
		CrearConvocatoriaComponent, 
		SeguimientoComponent
	]
})
export class AdministradorModule { }
