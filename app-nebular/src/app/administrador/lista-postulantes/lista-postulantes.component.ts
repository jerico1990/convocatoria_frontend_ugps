import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NbToastrService, NbMenuItem } from '@nebular/theme';
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from '@angular/router';
import { ConvocatoriaService } from 'src/app/service/convocatoria.service';
import { environment } from 'src/environments/environment';

import Swal from 'sweetalert2/dist/sweetalert2.js';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
	selector: 'app-lista-postulantes',
	templateUrl: './lista-postulantes.component.html',
	styleUrls: ['./lista-postulantes.component.scss']
})
export class ListaPostulantesComponent implements OnInit {
	grupoFiltro: FormGroup;
	submitted = false;
	listConvocatorias = []
	listEstado = []
	selectedItem
	totalCarga = 0
	// dtOptions: DataTables.Settings = {};
	urlDescarga = environment.API + 'api/convocatoria/descarga-archivo/';

	constructor(
		private formBuilder: FormBuilder,
		private toastrService: NbToastrService,
		private spinner: NgxSpinnerService,
		private router: Router,
		private convocatoria: ConvocatoriaService,
	) { }

	ngOnInit(): void {
		let usuario = sessionStorage.getItem('token')
		if (typeof usuario !== 'undefined' && usuario == null) {
			// this.router.navigateByUrl("/");
			this.router.navigate(["/"]);
		}
		this.validFormulario()
		this.loadDataConvocaria()
	}

	validFormulario() {
		this.grupoFiltro = this.formBuilder.group({
			descripcion: ['', Validators.required],
			estado: ['', Validators.required]
		});
	}
	get f() { return this.grupoFiltro.controls; }

	async loadDataConvocaria() {
		this.spinner.show()

		await this.convocatoria.listaConvocatoriaInterna().subscribe((data: any) => {
			// console.log(data.datos)
			// return
			data.datos.forEach((list: any) => {
				this.listConvocatorias.push({
					codigo: list.codConvocatoria,
					descripcion: list.txtDescripcion,
					aviso: list.txtAvisoArchivo,
					pdf: list.txtTdrArchivo,
					formato: list.txtFormatosArchivo,
					fechaInicio: list.fecFechaInicio,
					fechaFin: list.fecFechaFin,
					trazabilidad: list.txtDescripcionTrazabilidad,
					estado: list.txtDescripcionPublicado,
					postulantes: list.cantPostulantes,
				})
			});
			this.dataTable()
			this.spinner.hide();
		},
			(error) => {
				this.toastrService.danger("Error en la carga de convocatoria " + error.statusText, "Error");
				if (error instanceof HttpErrorResponse) {
					if (error.error instanceof ErrorEvent) {
						console.error("Error Event");
					} else {
						console.log(`error status : ${error.status} ${error.statusText}`);
						// console.log(error.error)
						this.spinner.hide();
						switch (error.status) {
							case 400:
								console.log(error)
								break;
							case 401:      //login
								this.router.navigateByUrl("/");
								break;
							case 403:     //forbidden
								// this.router.navigateByUrl("/unauthorized");
								break;
						}
					}
				} else {
					// console.error("some thing else happened");
				}
			});

		this.convocatoria.listaTrazabilidad().subscribe((data: any) => {
			// console.log(data)
			// return 
			data.datos.forEach((list: any) => {
				this.listEstado.push({
					codigo: list.codTrazabilidad,
					descripcion: list.txtDescripcion
				})
			});
			this.totalCarga = this.totalCarga + 1
		},
			(error) => {
				console.log(error)
				this.toastrService.danger("Error en la carga de estado " + error.statusText, "Error");
				this.spinner.hide();
			});

		// this.spinner.hide();

	}

	dtOptions: any = {};
	dataTable() {
		this.dtOptions = {
			pagingType: 'full_numbers',
			pageLength: 3,
			processing: true,
			dom: 'Bfrtip',
			buttons: [
				'excel'
			]
		};

		$(function () {
			var confing = {
				ordering: false,
				dom: 'Bfrtip',
				buttons: [
					'excel'
				],
				language: {
					emptyTable: '',
					zeroRecords: 'No hay coincidencias',
					lengthMenu: 'Mostrar _MENU_ elementos',
					search: 'Buscar:',
					info: 'De _START_ a _END_ de _TOTAL_ elementos',
					infoEmpty: 'De 0 a 0 de 0 elementos',
					infoFiltered: '(filtrados de _MAX_ elementos totales)',
					paginate: {
						first: 'Prim.',
						last: 'Últ.',
						next: 'Sig.',
						previous: 'Ant.'
					},
				},
			}
			$('#tablaLists').DataTable(confing);

		});
	}

	filtrarRegistros() {
		this.spinner.show();
		this.submitted = true;
		if (!this.grupoFiltro.invalid) {
			let conv = this.grupoFiltro.controls['descripcion'].value
			console.log(conv)
			$('#tablaLists').DataTable().column(1).search(conv).draw();

			let estado = this.grupoFiltro.controls['estado'].value
			$('#tablaLists').DataTable().column(6).search(estado).draw();
			this.spinner.hide();
			return
		}
		// setTimeout(() => {
		// }, 2000);
		this.spinner.hide();
		this.toastrService.danger("Ocurrio un error", "Archivo");
	}

	verDocumento(data) {
		// this.urlDescarga+data
		window.open(this.urlDescarga + data, '_blank');
		this.toastrService.warning("Ver documento : " + data, "Archivo PDF");
	}

	postularParticipante(data) {
		this.router.navigate(['postular/' + data.codigo]);
	}

	editarPostularParticipante(obj) {
		this.router.navigate(['editar-convocatoria/' + obj.codigo]);
	}

	verSeguimientoParticipante(obj) {
		this.router.navigate(['seguimiento-convocatoria/' + obj.codigo]);
	}

	crearConvocatoria($myParam: string = ''): void {
		const navigationDetails: string[] = ['/crear-convocatoria'];
		if ($myParam.length) {
			navigationDetails.push($myParam);
		}
		this.router.navigate(navigationDetails);
	}

	eliminarConvocatoria(obj) {
		Swal.fire({
			title: 'Esta seguro que desea eliminar el registro?',
			text: 'Debe de confirmar la eliminacion del registro',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Si, Eliminar',
			cancelButtonText: 'No'
		}).then((result) => {
			if (result.value) {

				this.convocatoria.eliminarConvocatoria({ "codConvocatoria": obj.codigo }).subscribe((data: any) => {
					Swal.fire(
						'Eliminado!',
						'Se elimino el registro correctamente!',
						'success'
					)
					window.location.reload()
					this.spinner.hide();
				},
					(error) => {
						console.log(error)
						this.toastrService.danger("Error en la carga de convocatoria " + error.statusText, "Error");
						this.spinner.hide();
					});


			} else if (result.dismiss === Swal.DismissReason.cancel) {
				Swal.fire(
					'Cancelled',
					'Product still in our database.)',
					'error'
				)
			}
		})
	}

}
