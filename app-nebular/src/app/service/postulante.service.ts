import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PostulanteService {
  API_URL       = environment.API+'api/';

  constructor(
    private httpClient: HttpClient
  ) { }


  registroPostulante(data){
		return this.httpClient.post( this.API_URL+'postulante/registrar' , data );
	}

  consultaDNI(data){
		return this.httpClient.post( 'https://fspreset.minagri.gob.pe:5001/consultadni' , data );
	}

  adjudicacionConvocatoria(data){
    let token =sessionStorage.getItem("token");
		let headers = new HttpHeaders().set("Authorization", `Bearer ${token}`)
		return this.httpClient.post( this.API_URL+'convocatoria/actualizar-finalizar' , data, { headers: headers } );
	}

  adjudicacionPostulante(data){
    let token =sessionStorage.getItem("token");
		let headers = new HttpHeaders().set("Authorization", `Bearer ${token}`)
		return this.httpClient.post( this.API_URL+'convocatoria/adjudicadar-postulante' , data, { headers: headers } );
	}

  regionPostulante()
  {
    return this.httpClient.get( this.API_URL+'ubigeo/lista-regiones' );
  }

  regionProvincia( data )
  {
    return this.httpClient.get( this.API_URL+'ubigeo/lista-provincias/' + data );
  }

  regionDistrito( data )
  {
    return this.httpClient.get( this.API_URL+'ubigeo/lista-distritos/' + data );
  }

  

}
