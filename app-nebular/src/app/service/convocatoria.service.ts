import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
// import { environment } from 'src/environments/environment';

@Injectable({
	providedIn: 'root'
})
export class ConvocatoriaService {
	API_URL       = environment.API+'api/';
	constructor(
		private httpClient: HttpClient
	) { }

	listaConvocatoriaId(data){
		return this.httpClient.get( this.API_URL+'convocatoria/consultar/' + data.id );
	}

	crearConvocatoria(data){
		let token =sessionStorage.getItem("token");
		let headers = new HttpHeaders().set("Authorization", `Bearer ${token}`)
		return this.httpClient.post( this.API_URL+'convocatoria/registrar' , data ,{ headers: headers });
	}

	updateConvocatoria(data){
		let token =sessionStorage.getItem("token");
		let headers = new HttpHeaders().set("Authorization", `Bearer ${token}`)
		return this.httpClient.post( this.API_URL+'convocatoria/actualizar' , data ,{ headers: headers } );
	}

	listaConvocatoriaInterna(){
		let token =sessionStorage.getItem("token");
		let headers = new HttpHeaders().set("Authorization", `Bearer ${token}`)
		return this.httpClient.get( this.API_URL+'convocatoria/lista-interna',{ headers: headers });
	}

	listaConvocatoriaTrazabilidad(){
		return this.httpClient.get( this.API_URL+'trazabilidad/lista-interna/CONVOCATORIA');
	}

	listaConvocatoriaPublica(){
		return this.httpClient.get( this.API_URL+'convocatoria/lista-publica');
	}

	listaTrazabilidad(){
		return this.httpClient.get( this.API_URL+'trazabilidad/lista-publica');
	}

	listaPostulante(data){
		return this.httpClient.get( this.API_URL+'postulante/lista/' + data.id );
	}

	eliminarConvocatoria(data){
		let token =sessionStorage.getItem("token");
		let headers = new HttpHeaders().set("Authorization", `Bearer ${token}`)
		return this.httpClient.post( this.API_URL+'convocatoria/eliminar',data, { headers: headers });
	}

	login(data){
		return this.httpClient.post( this.API_URL+'autenticacion/login' , data );
	}

}
