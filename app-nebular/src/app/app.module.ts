import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { NbThemeModule, NbLayoutModule } from '@nebular/theme';
import { NbThemeModule, NbLayoutModule, NbSidebarModule, NbCardModule, NbButtonModule, NbActionsModule, NbIconModule, NbInputModule, NbFormFieldModule, NbStepperModule, NbAlertModule, NbDialogModule, NbBadgeModule, NbTooltipModule, NbCalendarRangeModule, NbCalendarModule, NbListModule, NbUserModule, NbTabsetModule, NbSpinnerModule, NbToastrModule, NbMenuModule, NbRadioModule, NbSelectModule, NbCheckboxModule, NbAccordionModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from "ngx-spinner";

import { AdministradorModule } from './administrador/administrador.module';
import { ConvocatoriasModule } from './convocatorias/convocatorias.module';
import { SharedModule } from './shared/shared.module';
@NgModule({
	declarations: [
		AppComponent,
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		NbThemeModule.forRoot({ name: 'corporate' }),
		NbLayoutModule,
		NbEvaIconsModule,
		AppRoutingModule,
		NgxSpinnerModule,
		NbSidebarModule.forRoot(),
		NbCardModule,
		NbButtonModule,
		NbActionsModule,
		NbIconModule,
		NbInputModule,
		HttpClientModule,
		NbFormFieldModule,
		NbStepperModule,
		FormsModule,
		ReactiveFormsModule,
		NbAlertModule,
		NbDialogModule.forRoot({
			closeOnBackdropClick : false
		}),
		NbBadgeModule,
		NbTooltipModule,
		NbCalendarRangeModule,
		NbCalendarModule,
		NbListModule,
		NbUserModule,
		NbTabsetModule,
		NbSpinnerModule,
		NbToastrModule.forRoot({
			duration: 5000,
		}),
		NbMenuModule.forRoot(),
		NbRadioModule,
		NbSelectModule,
		NbCheckboxModule,
		NbAccordionModule,

		// SharedModule,
		AdministradorModule,
		ConvocatoriasModule,

	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
